package com.zarbosoft.alligatoroid.compiler.mortar.builtinother;

import com.zarbosoft.alligatoroid.compiler.Meta;

public class StaticMethodMeta {
  public final Meta.FuncInfo funcInfo;

  public StaticMethodMeta(Meta.FuncInfo funcInfo) {
    this.funcInfo = funcInfo;
  }
}
