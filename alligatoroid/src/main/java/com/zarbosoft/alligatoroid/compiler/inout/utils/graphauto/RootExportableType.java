package com.zarbosoft.alligatoroid.compiler.inout.utils.graphauto;

import com.zarbosoft.alligatoroid.compiler.inout.graph.ExportableType;
import com.zarbosoft.alligatoroid.compiler.mortar.graph.SingletonBuiltinExportable;

public interface RootExportableType extends ExportableType, SingletonBuiltinExportable {
}
